import React from 'react';
import ReactDOM from 'react-dom';
import { onSnapshot } from 'mobx-state-tree';
import { Provider } from 'mobx-react';
import makeInspectable from 'mobx-devtools-mst';
// import { connectReduxDevtools } from 'mst-middlewares';
// import remotedev from 'remotedev';

// CSS
import 'normalize.css';
import 'todomvc-common/base.css';
import 'todomvc-app-css/index.css';
import './index.css';

import TodoMVC from './components/TodoMVC';
import Store from './models/Store';

let savedTodos = localStorage.getItem('todos');
if (savedTodos) {
  savedTodos = JSON.parse(savedTodos);
} else {
  savedTodos = [];
}

let store = Store.create({ todos: savedTodos });
makeInspectable(store);
// connectReduxDevtools(remotedev, store);

// listen to new snapshots
onSnapshot(store, (snapshot) => {
  localStorage.setItem('todos', JSON.stringify(snapshot.todos));
});

ReactDOM.render(
  <Provider store={store}>
    <TodoMVC />
  </Provider >, document.getElementById('root'));
