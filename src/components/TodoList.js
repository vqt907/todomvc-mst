import React from 'react';
import { observer, inject } from 'mobx-react';
import TodoItem from './TodoItem';

@inject('store')
@observer
class TodoList extends React.Component {
  constructor(props) {
    super(props);

    this.toggleAll = this.toggleAll.bind(this);
  }

  toggleAll(e) {
    let checked = e.target.checked;
    this.props.store.toggleAll(checked);
  }

  render() {
    return (
      <section className="main">
        <input
          id="toggle-all"
          className="toggle-all"
          type="checkbox"
          defaultChecked={this.props.store.isAllDone}
          onChange={this.toggleAll}
        />
        <label
          htmlFor="toggle-all"
        />
        <ul className="todo-list">
          {
            this.props.store.displayTodos.map((todo) => {
              return (
                <TodoItem
                  key={todo.id}
                  todo={todo}
                />
              );
            })
          }
        </ul>
      </section>
    );
  }
}

export default TodoList;