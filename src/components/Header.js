import React from 'react';
import { inject } from 'mobx-react';

@inject('store')
class Header extends React.Component {
  constructor(props) {
    super(props);

    this.handleNewTaskKeyDown = this.handleNewTaskKeyDown.bind(this);
  }

  submitNewTask(task) {
    this.props.store.addTask(task);
  }

  handleNewTaskKeyDown(e) {
    if (e.keyCode !== 13) {
      return;
    }
    e.preventDefault();

    var val = e.target.value.trim();

    if (val) {
      this.submitNewTask(val);
      e.target.value = '';
    }
  }

  render() {
    return (
      <header className="header">
        <h1>todos</h1>
        <input
          className="new-todo"
          placeholder="What needs to be done?"
          onKeyDown={this.handleNewTaskKeyDown}
          autoFocus={true}
        />
      </header>
    );
  }
}

export default Header;