import React from 'react';
import classNames from 'classnames';
import { observer } from 'mobx-react';
import { observable } from 'mobx';

@observer
class TodoItem extends React.Component {
  @observable editing = false;

  taskInput = React.createRef();

  componentDidUpdate() {
    if (this.editing) {
      this.taskInput.current.focus();
    }
  }

  handleEdit = () => {
    this.editing = true;
  }

  handleSubmit = () => {
    this.editing = false;
    this.props.todo.update(this.taskInput.current.value);
  }

  handleDestroy = () => {
    this.props.todo.remove();
  }

  handleKeyDown = (e) => {
    if (e.keyCode !== 13) {
      return;
    }
    e.preventDefault();
    this.handleSubmit();
  }

  render() {
    return (
      <li className={classNames({
        completed: this.props.todo.done,
        editing: this.editing
      })}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.todo.done}
            onChange={this.props.todo.toggle}
          />
          <label onDoubleClick={this.handleEdit}>
            {this.props.todo.title}
          </label>
          <button className="destroy" onClick={this.handleDestroy} />
        </div>
        <input
          ref={this.taskInput}
          className="edit"
          defaultValue={this.props.todo.title}
          onBlur={this.handleSubmit}
          onKeyDown={this.handleKeyDown}
        />
      </li>
    );
  }
}

export default TodoItem;