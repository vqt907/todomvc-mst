import React from 'react';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';
import { pluralize } from '../utils';

@inject('store')
@observer
class Footer extends React.Component {
  render() {
    let completedTodosCount = this.props.store.activeTodos.length;
    let activeTodoWord = pluralize('item', completedTodosCount);
    let currentFilter = this.props.store.filter;
    return (
      <footer className="footer">
        <span className="todo-count">
          <strong>{completedTodosCount}</strong> {activeTodoWord} left
        </span>
        <ul className="filters">
          <li>
            <a
              onClick={this.props.store.showAll}
              className={classNames({ selected: currentFilter === -1 })}>
              All
            </a>
          </li>
          &nbsp;
          <li>
            <a
              onClick={this.props.store.showActive}
              className={classNames({ selected: currentFilter === 0 })}>
              Active
            </a>
          </li>
          &nbsp;
          <li>
            <a
              onClick={this.props.store.showDone}
              className={classNames({ selected: currentFilter === 1 })}>
              Completed
            </a>
          </li>
        </ul>
        <button
          className="clear-completed"
          onClick={this.props.store.clearCompleted}
        >
          Clear completed
        </button>
      </footer>
    );
  }
}

export default Footer;