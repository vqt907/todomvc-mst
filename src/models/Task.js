import { types, getRoot } from 'mobx-state-tree';

const Task = types.model('Task', {
  id: types.identifier(types.string),
  title: types.string,
  done: false
}).actions(self => ({
  toggle() {
    self.done = !self.done;
  },
  update(title) {
    if (title) {
      self.title = title;
    }
  },
  remove() {
    getRoot(self).removeTask(self);
  }
}));

export default Task;