import { types, destroy } from 'mobx-state-tree';
import uuid from 'uuid/v4';
import Task from './Task';

const Store = types.model('Store', {
  todos: types.array(Task),
  filter: -1
}).views(self => {
  return {
    get completedTodos() {
      return self.todos.filter(t => t.done === true);
    },
    get activeTodos() {
      return self.todos.filter(t => t.done === false);
    },
    get displayTodos() {
      switch (self.filter) {
        case 0:
          return self.activeTodos;
        case 1:
          return self.completedTodos;
        default:
          return self.todos;
      }
    },
    get isAllDone() {
      return self.todos.length > 0 && self.todos.length === self.completedTodos.length;
    }
  };
}).actions(self => ({
  showAll() {
    self.filter = -1;
  },
  showDone() {
    self.filter = 1;
  },
  showActive() {
    self.filter = 0;
  },
  addTask(title) {
    let newTask = Task.create({ id: uuid(), title: title });
    self.todos.push(newTask);
  },
  removeTask(task) {
    destroy(task);
  },
  toggleAll(done) {
    self.todos.forEach(task => task.done = done);
  },
  clearCompleted() {
    self.todos.replace(self.todos.filter(todo => todo.done === false));
  }
}));

export default Store;